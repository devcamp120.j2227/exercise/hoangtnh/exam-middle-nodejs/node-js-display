const getCurrentTime  = (request, response, next) =>{
    console.log("Middleware Current time: ", new Date().toLocaleString('en-US', { timeZone: 'Asia/Ho_Chi_Minh' }));
    next();
}
const getUrl = (request, response, next) =>{
    console.log( request.headers.host);
    next();
}

module.exports ={
    getCurrentTime,
    getUrl
}