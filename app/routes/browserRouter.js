//import express js
const express = require("express");

//Khai báo router
const router = express.Router();


//khởi tạo app 
const app = express() ;

//import middleware
const browserMiddleware = require("../middleware/browserMiddleware");

router.get("/", browserMiddleware.getCurrentTime);
router.get("/", browserMiddleware.getUrl);

module.exports = router;
